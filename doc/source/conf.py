import os
import sys

sys.path.insert(0, os.path.abspath('.'))
source_suffix = '.rst'
master_doc = 'index'
project = 'OpenDev Engagement'
copyright = ('OpenDev Contributors')
exclude_patterns = ['_build']
pygments_style = 'sphinx'
html_static_path = ['_static/']
html_theme = 'alabaster'
html_theme_options = {'logo': 'opendev.svg'}
