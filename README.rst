OpenDev Engagement
==================

Tooling to generate coarse-grained reports of aggregate
collaboration activity from publicly available APIs and archives
provided by OpenDev hosted services.
